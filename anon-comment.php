<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://amirhamzah.ninja
 * @since             1.0.0
 * @package           Anon_Comment
 *
 * @wordpress-plugin
 * Plugin Name:       Anon Comment
 * Plugin URI:        https://bitbucket.org/amirbujang/anon-comment
 * Description:       Allow logged in user to hide username in comment. 
 * Version:           1.0.0
 * Author:            Amir Hamzah
 * Author URI:        http://amirhamzah.ninja
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       anon-comment
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-anon-comment-activator.php
 */
function activate_anon_comment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-anon-comment-activator.php';
	Anon_Comment_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-anon-comment-deactivator.php
 */
function deactivate_anon_comment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-anon-comment-deactivator.php';
	Anon_Comment_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_anon_comment' );
register_deactivation_hook( __FILE__, 'deactivate_anon_comment' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-anon-comment.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_anon_comment() {

	$plugin = new Anon_Comment();
	$plugin->run();

}
run_anon_comment();
