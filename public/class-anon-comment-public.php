<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://amirhamzah.ninja
 * @since      1.0.0
 *
 * @package    Anon_Comment
 * @subpackage Anon_Comment/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Anon_Comment
 * @subpackage Anon_Comment/public
 * @author     Amir Hamzah <2anaklelakilaut@gmail.com>
 */
class Anon_Comment_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Anon_Comment_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Anon_Comment_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/anon-comment-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Anon_Comment_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Anon_Comment_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/anon-comment-public.js', array( 'jquery' ), $this->version, false );

	}
	
	
	public function add_anonimize_checkbox($comment_id) {
		if(empty($_POST['anonimize'])) {
			$_POST['anonimize'] = 1;
		}
		echo '<p class="comment-form-anonimize"><input type="checkbox" name="anonimize" checked="'.$_POST['anonimize'].'"/> Hide Username</p>';
	}
	
	public function store_comment_metadata($comment_id) {
		if(isset($_POST['anonimize']) && $_POST['anonimize'] == 'on') {
			$_POST['anonimize'] = 1;
		} else {
			$_POST['anonimize'] = 0;
		}

		global $wpdb;
		$table_name = $wpdb->prefix . 'comments';		
		$wpdb->update($table_name, array('comment_anonymous'=>$_POST['anonimize']), array('comment_ID'=>$comment_id));		
	}
	
	public function hide_comment_author($comment) {
		if($comment->comment_anonymous) {
			$comment->comment_author = 'Anonymous';
			$comment->comment_author_email = null;
		}
		
		return $comment;
	}
	
}
