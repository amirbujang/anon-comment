<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://amirhamzah.ninja
 * @since      1.0.0
 *
 * @package    Anon_Comment
 * @subpackage Anon_Comment/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Anon_Comment
 * @subpackage Anon_Comment/includes
 * @author     Amir Hamzah <2anaklelakilaut@gmail.com>
 */
class Anon_Comment_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
