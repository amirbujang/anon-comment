<?php

/**
 * Fired during plugin activation
 *
 * @link       http://amirhamzah.ninja
 * @since      1.0.0
 *
 * @package    Anon_Comment
 * @subpackage Anon_Comment/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Anon_Comment
 * @subpackage Anon_Comment/includes
 * @author     Amir Hamzah <2anaklelakilaut@gmail.com>
 */
class Anon_Comment_Activator {

	private static function has_anonymous_column() {
		global $wpdb;
		$db_name = DB_NAME;
		$table_name = $wpdb->prefix . 'comments';
		$query = "SELECT COUNT(*) as count FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '{$db_name}' AND TABLE_NAME = '{$table_name}' AND COLUMN_NAME = 'anonymous'";
		$row = $wpdb->get_row($query, OBJECT, 0);
		
		if($row->count && $row->count == 1)	{
			return true;
		} else {
			return false;
		}
	}
	
	private static function add_anonymous_column() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'comments';
		$query = "ALTER TABLE {$table_name} ADD COLUMN comment_anonymous INT(11) NULL DEFAULT '0'";

		$wpdb->query($query);
	}

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		if(!self::has_anonymous_column()) {
			self::add_anonymous_column();
		}
	}

}
